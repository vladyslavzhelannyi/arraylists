package com.listsHomework.aListTestData;

import com.listsHomework.implementations.AList2;

public class AList2TestData {

    public static final Integer[] originalList1 = new Integer[] {null, null, null, null, null, null, null, null, null,
            null};

    public static final Integer[] originalList2 = new Integer[] {13, 34, 21, null, null, null, null, null, null, null};

    public static final AList2<Integer> originalAList1 = new AList2<>();

    public static final AList2<Integer> originalAList2 = new AList2<>( originalList2);

    public static final Integer[] addList1 = new Integer[] {15, null, null, null, null, null, null, null, null,
            null};
    public static final Integer[] addList2 = new Integer[] {13, 34, 21, 20, null, null, null, null, null, null};

    public static final AList2<Integer> addAList1 = new AList2<>(addList1);

    public static final AList2<Integer> addAList2 = new AList2<>(addList2);

    public static final Integer[] addByIndexList1 = new Integer[] {10, null, null, null, null, null, null, null, null,
            null};
    public static final Integer[] addByIndexList2 = new Integer[] {13, 34, 25, 21, null, null, null, null, null, null};

    public static final AList2<Integer> addByIndexAList1 = new AList2<>(addByIndexList1);

    public static final AList2<Integer> addByIndexAList2 = new AList2<>(addByIndexList2);

    public static final Integer[] removeList1 = new Integer[] {34, 21, null, null, null, null, null, null, null, null};

    public static final Integer[] removeList2 = new Integer[] {13, 34, null, null, null, null, null, null, null, null};

    public static final AList2<Integer> removeAList1 = new AList2<>(removeList1);

    public static final AList2<Integer> removeAList2 = new AList2<>(removeList2);

    public static final Integer[] removeByIndexList1 = new Integer[] {34, 21, null, null, null, null, null, null, null,
            null};

    public static final Integer[] removeByIndexList2 = new Integer[] {13, 34, null, null, null, null, null, null, null,
            null};

    public static final AList2<Integer> removeByIndexAList1 = new AList2<>(removeByIndexList1 );

    public static final AList2<Integer> removeByIndexAList2 = new AList2<>(removeByIndexList2);

    public static final Integer[] setList1 = new Integer[] {13, 22, 21, null, null, null, null, null, null, null};

    public static final Integer[] setList2 = new Integer[] {13, 34, 11, null, null, null, null, null, null, null};

    public static final AList2<Integer> setAList1 = new AList2<>(setList1);

    public static final AList2<Integer> setAList2 = new AList2<>(setList2);

    public static final Integer[] removeAllList1 = new Integer[] {34, null, null, null, null, null, null, null, null,
            null};

    public static final Integer[] removeAllList2 = new Integer[] {null, null, null, null, null, null, null, null, null,
            null};

    public static final AList2<Integer> removeAllAList1 = new AList2<>(removeAllList1);

    public static final AList2<Integer> removeAllAList2 = new AList2<>(removeAllList2);

}
